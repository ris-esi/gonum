package mat

import (
	"math/cmplx"

	"gonum.org/v1/gonum/blas"
	"gonum.org/v1/gonum/blas/cblas128"
	"gonum.org/v1/gonum/cmplxs"
	"gonum.org/v1/gonum/lapack"
	"gonum.org/v1/netlib/lapack/lapacke"
)

// LU is a type for creating and using the LU factorization of a matrix.
type cLU struct {
	lu    *CDense
	pivot []int
	cond  float64
}

// updateCond updates the stored condition number of the matrix. anorm is the
// norm of the original matrix. If anorm is negative it will be estimated.
func (clu *cLU) updateCond(anorm float64, norm lapack.MatrixNorm) {
	n := clu.lu.mat.Cols
	work := getFloat64s(4*n, false)
	defer putFloat64s(work)
	rcond := make([]float64, 2*n)
	if anorm < 0 {
		// This is an approximation. By the definition of a norm,
		//  |AB| <= |A| |B|.
		// Since A = L*U, we get for the condition number κ that
		//  κ(A) := |A| |A^-1| = |L*U| |A^-1| <= |L| |U| |A^-1|,
		// so this will overestimate the condition number somewhat.
		// The norm of the original factorized matrix cannot be stored
		// because of update possibilities.
		u := cblas128.Triangular{
			N:      n,
			Stride: clu.lu.mat.Stride,
			Data:   clu.lu.mat.Data,
			Uplo:   blas.Upper,
			Diag:   blas.NonUnit,
		}
		l := cblas128.Triangular{
			N:      n,
			Stride: clu.lu.mat.Stride,
			Data:   clu.lu.mat.Data,
			Uplo:   blas.Lower,
			Diag:   blas.Unit,
		}
		unorm := lapacke.Zlantr(byte(norm), byte(u.Uplo), byte(u.Diag), u.N, u.N, u.Data, max(1, u.Stride), work)
		lnorm := lapacke.Zlantr(byte(norm), byte(l.Uplo), byte(l.Diag), l.N, l.N, l.Data, max(1, l.Stride), work)
		anorm = unorm * lnorm
	}
	ok := lapacke.Zgecon(byte(norm), n, clu.lu.mat.Data, max(1, clu.lu.mat.Stride), anorm, rcond, make([]complex128, 2*n), make([]float64, 2*n))

	if ok {
		clu.cond = 1 / rcond[0]
	}
}

// Factorize computes the LU factorization of the square matrix a and stores the
// result. The LU decomposition will complete regardless of the singularity of a.
//
// The LU factorization is computed with pivoting, and so really the decomposition
// is a PLU decomposition where P is a permutation matrix. The individual matrix
// factors can be extracted from the factorization using the Permutation method
// on Dense, and the LU.LTo and LU.UTo methods.
func (clu *cLU) Factorize(a CMatrix) {
	clu.factorize(a, CondNorm)
}

func (clu *cLU) factorize(a CMatrix, norm lapack.MatrixNorm) {
	r, c := a.Dims()

	if r != c {
		panic(ErrSquare)
	}
	if clu.lu == nil {
		clu.lu = NewCDense(r, r, nil)
	} else {
		clu.lu.Reset()
		clu.lu.reuseAsNonZeroed(r, r)
	}
	clu.lu.Copy(a)
	pivot := make([]int32, r)
	if cap(clu.pivot) < r {
		clu.pivot = make([]int, r)
	}
	work := getFloat64s(r, false)
	anorm := lapacke.Zlange(byte(norm), r, r, clu.lu.mat.Data, max(1, clu.lu.mat.Stride), work)
	putFloat64s(work)
	ok := lapacke.Zgetrf(r, r, clu.lu.mat.Data, max(1, clu.lu.mat.Stride), pivot)
	if ok {
		for i, v := range pivot {
			clu.pivot[i] = int(v)
		}
		clu.updateCond(anorm, norm)
	}

}

// isValid returns whether the receiver contains a factorization.
func (clu *cLU) isValid() bool {
	return clu.lu != nil && !clu.lu.IsEmpty()
}

// Pivot returns pivot indices that enable the construction of the permutation
// matrix P (see Dense.Permutation). If swaps == nil, then new memory will be
// allocated, otherwise the length of the input must be equal to the size of the
// factorized matrix.
// Pivot will panic if the receiver does not contain a factorization.
func (clu *cLU) Pivot(swaps []int) []int {
	if !clu.isValid() {
		panic(badLU)
	}

	_, n := clu.lu.Dims()
	if swaps == nil {
		swaps = make([]int, n)
	}
	if len(swaps) != n {
		panic(badSliceLength)
	}
	// Perform the inverse of the row swaps in order to find the final
	// row swap position.
	for i := range swaps {
		swaps[i] = i
	}
	for i := n - 1; i >= 0; i-- {
		v := clu.pivot[i] - 1
		swaps[i], swaps[v] = swaps[v], swaps[i]
	}
	return swaps
}

func (clu *cLU) UTo() CDense {
	if !clu.isValid() {
		panic(badLU)
	}

	_, n := clu.lu.Dims()
	dst := NewCDense(n, n, nil)
	// Extract the upper triangular elements.
	for i := 0; i < n; i++ {
		for j := i; j < n; j++ {
			dst.mat.Data[i*dst.mat.Stride+j] = clu.lu.mat.Data[i*clu.lu.mat.Stride+j]
		}
	}
	return *dst
}

func (clu *cLU) LTo() CDense {
	if !clu.isValid() {
		panic(badLU)
	}
	_, n := clu.lu.Dims()
	dst := NewCDense(n, n, nil)
	// Extract the lower triangular elements.
	for i := 0; i < n; i++ {
		for j := 0; j < i; j++ {
			dst.mat.Data[i*dst.mat.Stride+j] = clu.lu.mat.Data[i*clu.lu.mat.Stride+j]
		}
	}
	// Set ones on the diagonal.
	for i := 0; i < n; i++ {
		dst.mat.Data[i*dst.mat.Stride+i] = 1
	}
	return *dst
}

func (clu *cLU) Det() complex128 {
	det, sign := clu.LogDet()
	return cmplx.Exp(det) * complex(sign, 0)
}

// LogDet returns the log of the determinant and the sign of the determinant
// for the matrix that has been factorized. Numerical stability in product and
// division expressions is generally improved by working in log space.
// LogDet will panic if the receiver does not contain a factorization.
func (clu *cLU) LogDet() (det complex128, sign float64) {
	if !clu.isValid() {
		panic(badLU)
	}

	_, n := clu.lu.Dims()
	logDiag := make([]complex128, n)
	sign = 1.0
	for i := 0; i < n; i++ {
		v := clu.lu.at(i, i)
		if clu.pivot[i] != i {
			sign *= -1
		}
		logDiag[i] = cmplx.Log(v)
	}
	return cmplxs.Sum(logDiag), sign
}
